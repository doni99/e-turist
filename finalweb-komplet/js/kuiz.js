var instructions = document.getElementById("instructions");
var quiz = document.getElementById("quiz");
var startBtn = document.getElementById("startBtn");
var askQuestion = document.getElementById("askQuestion");
var submitBtn = document.getElementById("submitBtn");
var resetBtn = document.getElementById("resetBtn");
var form = document.getElementById("form");
var quiz = document.getElementById("quiz");
var inform = document.getElementById("inform");
var showScore = document.getElementById("showScore");
var displayScore = document.getElementById("displayScore");
var displayQCount = document.getElementById("displayQCount");
var checkedRadio;
var allRadios;
var i;
var score;

var questions = [
  {
    question: "Cili ekip ka fituar champions league-n ne vitin 2010?",
    choices: ["FC Barcelona", "Bayern", "Inter", "Real Madrid"],
    correct: 2
  },
  {
    question: "Cili lojtar 'tradhtoj' ekipin e tije per t'iu bashkuar rivaleve ne vitin 2000?",
    choices: ["Luis Enrique", "Thierry Henry", "Ronaldo(fenomeni)", "Luis Figo"],
    correct: 3
  },
  {
    question: "Lojtari me më se shumti nominime per topin e arte eshte?",
    choices: ["Cristiano Ronaldo", "Ronaldinho", "Lionel Messi", "Rivaldo"],
    correct: 0
  },
  {
    question: "Para se te behej me pasaporte te Spanjes , Diego Costa e kishte te ?",
    choices: ["Uruguajit", "Kilit", "Perus", "Brazilit"],
    correct: 3
  },
  {
    question: "Ekipi me i trofeshem ne futboll aktualisht eshte ?",
    choices: ["AC Milan", "Real Madrid", "FC Barcelona", "Bayern Munich"],
    correct: 2
  }
];

window.onload = beginQuiz;

function beginQuiz() {
    form.style.display = "block";
    instructions.style.display = "block";
    showScore.style.display = "none";
    quiz.style.display = "none";
    submitBtn.style.display = "none";
    i = 0;
    score = 0;
    displayQCount.innerHTML = 1;
    displayScore.innerHTML = 0;
}

startBtn.addEventListener("click", function() {
    instructions.style.display = "none";
    submitBtn.style.display = "block";
    quiz.style.display = "block";
    getQAs();
});

submitBtn.addEventListener("click", function() {
    allRadios = document.getElementsByName("option");
    var isChecked = false;
    for (var j = 0; j < allRadios.length; j++) {
        if (allRadios[j].checked) {
            isChecked = true;
            checkedRadio = j;
            break;
        }
    }
    if (!(isChecked)) {
        alert("Ju lutem zgjedhni nje opsion");
    } else {
        getResults();
        deselectRadios();
        i++;
        displayQCount.innerHTML = i + 1;
        getQAs();
    }
});

function deselectRadios() {
    allRadios = document.getElementsByName("option");
    for (var p = 0; p < allRadios.length; p++) {
        allRadios[p].checked = false;
    }
}

function getResults() {
        if (allRadios[checkedRadio].value === questions[i].choices[questions[i].correct]) {
            score++;
            displayScore.innerHTML = score;
        }
}

function getQAs() {
    if (i < 5) {
        askQuestion.innerHTML = questions[i].question;
        for (var k = 0; k < 4; k++) {
            document.getElementById("answer" + k).innerHTML = questions[i].choices[k];
            document.getElementById("answer" + k).setAttribute("for", questions[i].choices[k]);
            document.getElementById("label" + k).setAttribute("value", questions[i].choices[k]);
        }
    } else {
        displayResults();
    }
};

function displayResults() {
    quiz.style.display = "none";
    showScore.style.display = "block";
    inform.innerHTML = "Kuizi ka perfunduar!! Keni shenuar " + score + " pike prej 5.";
};

function ruajKomentin() {
  alert("Komenti eshte ruajtur me sukses, ju faleminderit!");
}

resetBtn.addEventListener("click", function() {
    beginQuiz();
});

